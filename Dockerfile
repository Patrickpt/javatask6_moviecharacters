FROM openjdk:15
ADD target/JavaTask6-0.0.1-SNAPSHOT.jar movies.jar
ENTRYPOINT ["java", "-jar" , "movies.jar"]