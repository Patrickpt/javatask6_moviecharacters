# MOVIE API

By Eirik Saur, Anette Olli Siiri, and Patrick Torkildsen.

## Description
This project offers an API and a database-schema to the user.
### DB
The database-schema will be created upon running this project, 
but the user will have to provide a postgre database themselves.
The schema contains tables for Characters, Movies and Franchises and the logic between them.
### API
The API provided through this project will enable queries to the database.
The queries are mostly CRUD, but there are also some extra queries.
Examples of queries and general use of the API can be found in the folder postman_collection.

### Database ER Diagram:
This is how the database should be generated:

![ER-diagram generated](images/ER_diagram.png)

### Setting up the project:
This project requires the following environment variables:

MOVIE_DB_URL: jdbc:postgresql://your_url:your_url/your_database

MOVIE_DB_USERNAM: your_username

MOVIE_DB_PASSWORD: your_password

On windows: editing environment variables requires reboot of the machine.

In heroku: adding environment variables in settings
![](images/heroku_settings.PNG)

## Pushing to heroku:

1. in intellij with maven: clean and package 
2. in bash:
```shell script
docker login
docker build . -t anetteos/movie-api
heroku container:login
heroku container:push web --app experis-movie-api
heroku container:release web --app experis-movie-api
```


## Link to heroku:
https://noroff-movie-api.herokuapp.com

## Link to postman:
https://documenter.getpostman.com/view/12300473/TVRedqVo