package com.noroff.JavaTask6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaTask6Application {

	public static void main(String[] args) {
		SpringApplication.run(JavaTask6Application.class, args);
	}

}
