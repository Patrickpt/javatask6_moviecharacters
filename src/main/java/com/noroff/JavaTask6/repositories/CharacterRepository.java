package com.noroff.JavaTask6.repositories;

import com.noroff.JavaTask6.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {

    Character getById(Long id);

}
