package com.noroff.JavaTask6.repositories;

import com.noroff.JavaTask6.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
    Franchise getById(Long id);
}
