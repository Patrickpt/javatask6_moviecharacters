package com.noroff.JavaTask6.repositories;

import com.noroff.JavaTask6.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Long> {


    Movie getById(Long id);

    //single parameter match:
    public List<Movie> findAllByTitle(String title);
    public List<Movie> findAllByDirector(String director);
    public List<Movie> findAllByGenre(String genre);


    //double parameter match:
    public List<Movie> findAllByTitleAndDirector(String title, String director);
    public List<Movie> findAllByTitleAndGenre(String title, String genre);
    public List<Movie> findAllByDirectorAndGenre(String director, String genre);

    //Triple parameter match:
    public List<Movie> findAllByTitleAndDirectorAndGenre(String title, String director, String genre);

}
