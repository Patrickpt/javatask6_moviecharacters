package com.noroff.JavaTask6.controllers;

import com.noroff.JavaTask6.models.Character;
import com.noroff.JavaTask6.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class CharacterController {

    @Autowired
    CharacterRepository charRepo;

    @RequestMapping(value = "/characters", method = RequestMethod.GET)
    public ResponseEntity<?> getAllCharacters(){
        return ResponseEntity.status(HttpStatus.OK).body(charRepo.findAll());
    }

    @RequestMapping(value = "/characters/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getCharacter(@PathVariable Long id){
        Character theCharacter = charRepo.getById(id);
        if(theCharacter == null){//Check that the character exists
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Id does not exist");
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(charRepo.getById(id));
        }
    }

    @RequestMapping(value = "/characters", method = RequestMethod.POST)
    public ResponseEntity<?> createCharacter(@RequestBody Character character){
        if(character.getFullName() == null){//Checking that it has a name as that field is required
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("The character need to have a name");
        }else{
            return ResponseEntity.status(HttpStatus.CREATED).body(charRepo.save(character));
        }
    }

    @RequestMapping(value = "/characters", method = RequestMethod.PUT)
    public ResponseEntity<?> updateCharacter(@RequestBody Character newCharacter){
        if(newCharacter.getFullName() == null){//Checking that it has a name as that field is required
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("The character need to have a name");
        }else if(charRepo.getById(newCharacter.getId()) != null){//Checkign that the character exists
            Character updatedCharacter = charRepo.save(newCharacter);
            return ResponseEntity.status(HttpStatus.OK).body(updatedCharacter);
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Id does not exist");
        }
    }

    @RequestMapping(value = "/characters/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteCharacter(@PathVariable Long id){
        Character theCharacter = charRepo.getById(id);//Getting the character to delete based on id
        if(theCharacter != null){//Checking that the character exists
            charRepo.delete(theCharacter);
            return ResponseEntity.status(HttpStatus.OK).body("Deleted");
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Id does not exist");
        }
    }

}
