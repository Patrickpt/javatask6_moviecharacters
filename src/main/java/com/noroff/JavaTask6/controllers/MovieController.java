package com.noroff.JavaTask6.controllers;

import com.noroff.JavaTask6.models.Franchise;
import com.noroff.JavaTask6.models.Movie;
import com.noroff.JavaTask6.repositories.FranchiseRepository;
import com.noroff.JavaTask6.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("/api/v1")
public class MovieController {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private FranchiseRepository franchiseRepository;

    /**
     *
     * @param title - optional, title of movie
     * @param genre - optional, genre of movie
     * @param director - optional, director of movie
     * @return status 200 and list of the specified movies
     */
    @RequestMapping(value = "/movies", method = RequestMethod.GET)
    public ResponseEntity<?> getAllMovies(@RequestParam(value = "title", required = false) String title,
                                          @RequestParam(value = "genre", required = false) String genre,
                                          @RequestParam(value = "director", required = false) String director){
        List<Movie> movies = null;
        //For optimazation this could have been written as nesten if's, but has not been done to increase readability.
        if (title != null && genre == null && director == null) {
            movies = movieRepository.findAllByTitle(title);
        }else if (title != null && genre != null && director == null) {
            movies = movieRepository.findAllByTitleAndGenre(title, genre);
        }else if (title != null && genre != null && director != null) {
            movies = movieRepository.findAllByTitleAndDirectorAndGenre(title, director, genre);
        }else if (title != null && genre == null && director != null) {
            movies = movieRepository.findAllByTitleAndDirector(title, director);
        }else if (title == null && genre != null && director == null) {
            movies = movieRepository.findAllByGenre(genre);
        }else if (title == null && genre != null && director != null) {
            movies= movieRepository.findAllByDirectorAndGenre(director, genre);
        }else if (title == null && genre == null && director != null) {
            movies = movieRepository.findAllByDirector(director);
        }else {
            movies = movieRepository.findAll();
        }

        return ResponseEntity.status(HttpStatus.OK).body(movies);




    }

    /**
     * Find a single movie specified by the id.
     * @param id of movie
     * @return status 200 and the movie specified by id. If movie cannot be found returns 404
     */
    @RequestMapping(value = "/movies/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getMovie(@PathVariable Long id){
        Movie movie = movieRepository.getById(id);
        if (movie != null) {
            return ResponseEntity.status(HttpStatus.OK).body(movie);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("movie with id " + id + " does not exists");
    }

    /**
     * Deletes a specified movie from det database.
     * @param id of movie
     * @return 200 if deleted, or 404 if the movie could not be found
     */
    @RequestMapping(value = "/movies/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteMovie(@PathVariable Long id){
        Movie found = movieRepository.getById(id);
        if(found == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Movie with id " + id + " does not exist");
        }
        movieRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).body("Movie with id " + id + " was deleted");
    }

    /**
     * Stores a movie to the database.
     * @param movie that will be added to the database
     * @return status 203 and the movie if everything went well. If parameters are missing it returns a description.
     */
    @RequestMapping(value =  "/movies", method = RequestMethod.POST)
    public ResponseEntity<?> postMovie(@RequestBody Movie movie) {
        if (movie.getId() != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("You cannot assign id");
        }else if (movie.getTitle() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing title");
        }else if (movie.getGenre() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing genre");
        }else if (movie.getYear() == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing year");
        }else if(movie.getDirector() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing director");
        }else if (movie.getPicture() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing picture");
        }else if (movie.getTrailer() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing trailer");
        }
        if(movie.getFranchise() != null){
            if(movie.getFranchise().getId() != null){
                Long existingFranchiseId = movie.getFranchise().getId();
                Franchise existingFranchise = new Franchise();
                if (franchiseRepository.existsById(existingFranchiseId)) {
                    existingFranchise = franchiseRepository.findById(existingFranchiseId).get();
                    movie.setFranchise(existingFranchise);
                }else{
                    movie.setFranchise(null);
                }

            }
        }
        Movie addedMovie = movieRepository.save(movie);
        return ResponseEntity.status(HttpStatus.CREATED).body(addedMovie);
    }

    /**
     * Updates a movie in the database
     * @param movie that will be updated
     * @return status 200 and the movie if everything went well.
     * If parameters are missing it returns 400 and description.
     * If movie does not exists it returns 404 not found and description.
     */
    @RequestMapping(value =  "/movies", method = RequestMethod.PUT)
    public ResponseEntity<?> updateMovie(@RequestBody Movie movie){

        if (movie.getId() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing id");
        }else if (movie.getTitle() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing title");
        }else if (movie.getGenre() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing genre");
        }else if (movie.getYear() == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing year");
        }else if(movie.getDirector() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing director");
        }else if (movie.getPicture() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing picture");
        }else if (movie.getTrailer() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing trailer");
        }
        Movie found = movieRepository.getById(movie.getId());
        if (found == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Movie with " + movie.getId() + " id, does not exist");
        }
        Movie updatedMovie = movieRepository.save(movie);
        return ResponseEntity.status(HttpStatus.OK).body(updatedMovie);
    }

    /**
     * Finds characters for a given movie
     * @param id of the movie
     * @return a list of characters and status code 200.
     * If movie does not exists it returns 404 and description.
     */
    @RequestMapping(value = "/movies/{id}/characters")
    public ResponseEntity<?> getCharactersForAMovie(@PathVariable Long id) {
        Movie movie = movieRepository.getById(id);
        if( movie != null) {
            return ResponseEntity.status(HttpStatus.OK).body(movie.getCharacters());
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("movie with id " + id + " does not exists");
        }

    }

}
