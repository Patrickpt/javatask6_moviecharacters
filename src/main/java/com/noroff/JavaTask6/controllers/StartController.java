package com.noroff.JavaTask6.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StartController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<?> startingPoint(){
        return ResponseEntity.status(HttpStatus.OK).body("welcome");
    }

}
