package com.noroff.JavaTask6.controllers;

import com.noroff.JavaTask6.models.Character;
import com.noroff.JavaTask6.models.Franchise;
import com.noroff.JavaTask6.models.Movie;
import com.noroff.JavaTask6.repositories.FranchiseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/v1/franchise")
public class FranchiseController {

    @Autowired
    private FranchiseRepository franchiseRepository;

    @RequestMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises, status);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id){
        Franchise franchise = new Franchise();
        HttpStatus status;
        if(franchiseRepository.existsById(id)){//Check that the franchise actually exists
            System.out.println("Found franchise with id " + id);
            status = HttpStatus.OK;
            franchise = franchiseRepository.findById(id).get();
        } else{
            System.out.println("Franchise not found");
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(franchise, status);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){
        System.out.println(franchise.getId());
        franchise = franchiseRepository.save(franchise);
        HttpStatus resp = HttpStatus.CREATED;
        System.out.println("Created new franchise with id " + franchise.getId());
        return new ResponseEntity<>(franchise, resp);
    }

    @RequestMapping( method = RequestMethod.PUT)
    public ResponseEntity<?> updateFranchise( @RequestBody Franchise franchise){
        HttpStatus status;
        Franchise found = franchiseRepository.getById(franchise.getId());
        if(found == null){//Checking if the franchise actually exists
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>("franchise with id " + franchise.getId() + " does not exists", status);
        }
        Franchise returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.OK;
        return new ResponseEntity<>(returnFranchise, status);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteFranchise(@PathVariable Long id){
        HttpStatus status;
        String message = "";
        if(franchiseRepository.existsById(id)){//Checking if franchise exists
            franchiseRepository.deleteById(id);
            System.out.println("Deleted franchise with id " + id);
            message = "Deleted franchise with id " + id;
            status = HttpStatus.OK;
        }else{
            System.out.println("Franchise with id " + id + " not found");
            message = "Franchise with id " + id + " not found";
            status = HttpStatus.NOT_FOUND;
        }
        return ResponseEntity.status(status).body(message);
    }

    @RequestMapping(value = "/{id}/movies", method = RequestMethod.GET)
    public ResponseEntity<?> getMoviesForAFranchise(@PathVariable Long id){
        Franchise theFranchise = franchiseRepository.getById(id);
        if(theFranchise == null){//Checking if the franchise exists
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Id does not exist");
        }else {
            return ResponseEntity.status(HttpStatus.OK).body(theFranchise.getMovies());
        }
    }


    @RequestMapping(value = "/{id}/characters", method = RequestMethod.GET)
    public ResponseEntity<?> getCharactersForAFranchise(@PathVariable Long id){
        Franchise theFranchise = franchiseRepository.getById(id);
        if(theFranchise == null){//Checking if the franchise exists
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Id does not exist");
        }else {
            //Getting the movies related to the franchise
            List<Movie> movieList = theFranchise.getMovies();
            List<Character> charList = new ArrayList<>();
            for (int i = 0; i < movieList.size(); i++) {
                //Getting the characters from every movie thats related to the franchise
                charList.addAll(movieList.get(i).getCharacters());
            }
            //Hacky way to remove duplicated from the list
            Set<Character> set = new HashSet<>(charList);
            charList.clear();
            charList.addAll(set);
            return ResponseEntity.status(HttpStatus.OK).body(charList);
        }
    }

}
